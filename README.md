# PHPayloads: serialized PHP generation

This library provides utilities for constructing serialized PHP objects,
primarily for the purpose of solving testing and solving CTF challenges.

Since it is intended primarily to generate payloads, no deserialization
capabilities exist. However, functions for modifying serialized data may
be added in the future.

## Building the project

`cargo build` should work. The project currently depends only on Rust's
standard libraries.

`cargo doc` will generate documentation pages locally. I recommend doing
this, since the docs are not currently hosted anywhere on the internet.

### As a dependency

Add `phpayloads = { git = "https://gitlab.com/evanj2357/phpayloads" }` to
the dependencies section of your project's `Cargo.toml`.

## Why not install PHP and use `serialize` directly?

I don't want to. Plus, this is as good an excuse as any to write more code
in my favorite language!
