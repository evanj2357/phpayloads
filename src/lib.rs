//! PHP serialized object generation utilities.
//!
//! These utilities are intended to help generate payloads for testing PHP insecure
//! deserialization vulnerabilities. Please use responsibly.
//!
//! As a result, rather than implement PHP serialization exactly, this crate attempts
//! to:
//! 1. make it easy to generate correct, valid PHP objects
//! 2. allow some flexibility to produce slightly "wrong" objects in case you need to
//!
//! Example
//! ```
//! use phpayloads;
//! use phpayloads::{Access, PHPType};
//!
//! let empty_point = phpayloads::Object::new("Point");
//!
//! let x1 = phpayloads::Field::new(Access::Public, "x", PHPType::Int(3));
//! let y1 = phpayloads::Field::new(Access::Public, "y", PHPType::Int(4));
//!
//! let point1 = empty_point.add_field(x1).add_field(y1);
//!
//! let serialized_point1 = point1.serialize();
//! assert_eq!(serialized_point1, "O:5:\"Point\":2:{s:1:\"x\";i:3;s:1:\"y\";i:4;}");
//! ```

#![deny(missing_docs)]

use indexmap::set::IndexSet;
use std::hash::Hash;
use std::hash::Hasher;
use std::ops::Add;

/// Approximate representations of PHP datatypes.
///
/// Some of these do not exactly conform to PHP's standard behavior.
/// Notably, `PHPType::Array` does not enforce uniqueness the way PHP's
/// associative arrays do.
#[derive(PartialEq, Clone, Debug)]
pub enum PHPType {
    /// Text data
    String(String),

    /// Integers, assuming a 64-bit target system. For 32-bit targets, the user
    /// is responsible for ensuring bounds are enforced as needed.
    Int(i64),

    /// Floating-point numbers, assuming a 64-bit target system
    Float(f64),

    /// True or False
    Boolean(bool),

    /// Approximation of PHP arrays. Use only distinct string or integer keys if
    /// you want correctness.
    Array(Vec<(PHPType, PHPType)>),

    /// Custom objects
    Object(Object),

    /// Handle null values separately
    Null,
}

/// Internal representation for fields of a PHP object
#[derive(Clone, Debug)]
pub struct Field {
    access: Access,
    name: String,
    value: PHPType,
}

/// Access specifiers for object fields. These are needed because PHP serializes
/// private and public data members slightly differently.
#[derive(PartialEq, Eq, Clone, Debug)]
pub enum Access {
    /// for fields that should be serialized as private
    Private,

    /// for public fields
    Public,
}

/// Representation of an instance of a PHP class.
#[derive(PartialEq, Eq, Clone, Debug)]
pub struct Object {
    class: String,
    fields: IndexSet<Field>,
}

impl PHPType {
    /// Generate a string representation of a PHP value, as if emitted by PHP's
    /// `serialize` function.
    pub fn serialize(&self) -> String {
        match self {
            Self::String(s) => s.len().to_string().add(":\"").add(&s).add("\""),
            Self::Int(n) => "i:".to_owned().add(&n.to_string()),
            Self::Float(n) => "f:".to_owned().add(&n.to_string()),
            Self::Boolean(b) => {
                let n = match b {
                    false => 0,
                    true => 1,
                };
                "b:".to_owned().add(&n.to_string())
            }
            Self::Array(arr) => {
                let contents = arr.iter().fold(String::new(), |acc, (key, itm)| {
                    acc.add(&key.serialize()).add(&itm.serialize())
                });
                "a:".to_owned()
                    .add(&arr.len().to_string())
                    .add("{")
                    .add(&contents)
                    .add("}")
            }
            Self::Object(o) => o.serialize(),
            Self::Null => "N".to_owned(),
        }
        .add(";")
    }
}

impl PartialEq for Field {
    fn eq(&self, other: &Self) -> bool {
        self.name == other.name
    }
}

impl Eq for Field {}

impl Hash for Field {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.name.hash(state);
    }
}

impl Field {
    /// Construct a new `Field` holding some value.
    pub fn new(access: Access, name: &str, value: PHPType) -> Self {
        Self {
            access,
            name: name.to_owned(),
            value,
        }
    }

    /// Generate a string representation of a field, as if emitted by PHP's
    /// `serialize` function.
    pub fn serialize(&self, parent_class: &str) -> String {
        let label = match &self.access {
            Access::Private => String::from("\0")
                .add(parent_class)
                .add("\0")
                .add(&self.name),
            Access::Public => self.name.clone(),
        };

        let value_data = self.value.serialize();

        let data = String::from("s:")
            .add(&label.len().to_string())
            .add(":\"")
            .add(&label)
            .add("\";")
            .add(&value_data);

        data
    }
}

impl Object {
    /// Instantiate an empty object with a class name.
    pub fn new(class: &str) -> Self {
        Self {
            class: class.to_owned(),
            fields: IndexSet::new(),
        }
    }

    /// Add a [`Field`] to the object and return the modified object.
    pub fn add_field(mut self, field: Field) -> Self {
        let _ = self.fields.insert(field);
        self
    }

    /// Generate a string representation of an [`Object`], as if emitted by PHP's
    /// `serialize` function.
    pub fn serialize(&self) -> String {
        let mut data = String::from("O:")
            .add(&self.class.len().to_string())
            .add(":\"")
            .add(&self.class)
            .add("\":")
            .add(&self.fields.len().to_string())
            .add(":{");

        for field in self.fields.iter() {
            data = data.add(&field.serialize(&self.class));
        }

        data.add("}")
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn serialize_simple_object() {
        let correct_representation = "O:5:\"MyObj\":1:{s:2:\"tf\";b:0;}";

        let object = Object::new("MyObj").add_field(Field::new(
            Access::Public,
            "tf",
            PHPType::Boolean(false),
        ));

        assert_eq!(&object.serialize(), correct_representation);
    }
}
